use std::{cell::RefCell, io, rc::Rc};

use nes_core::{debugger::Composite, Runner};
use nes_disassembler::NestestLog;

fn main() -> io::Result<()> {
	let mut debugger = Composite::new();
	debugger.add(Rc::new(RefCell::new(NestestLog::new("nestest.log")?)));

	let mut runner = Runner::with_debugger(debugger);
	runner.reset();
	for _ in 0..10240 {
		if runner.run() {
			eprintln!("HALTED");
			break;
		}
	}

	/*let mut bus = Bus::new();

	{
		let bus = unsafe { bus.as_mut().get_unchecked_mut() };

		let mut disassembler = SimpleDisassembler::new(bus);
		disassembler.mem_view(0xC004);
		disassembler.disassemble_rom();
	}*/

	/*bus.as_mut().reset();
	for _ in 0..1024 {
		bus.as_mut().run();
	}*/

	Ok(())
}
