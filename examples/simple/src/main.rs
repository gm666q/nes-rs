use std::{cell::RefCell, io, rc::Rc};

use crossterm::event::{self, Event, KeyCode, KeyEvent, KeyEventKind};
use nes_core::{debugger::Composite, Runner};
use nes_disassembler::{Memory, NestestLog, SimpleDisassembler, State};
use ratatui::{
	prelude::*,
	symbols::border,
	widgets::{block::*, *},
};

use crate::memory_view::MemoryView;

mod memory_view;
mod tui;

//#[derive(Debug, Default)]
pub struct App {
	disassembler: Rc<RefCell<SimpleDisassembler>>,
	exit: bool,
	memory: Rc<RefCell<Memory>>,
	page: Option<u8>,
	runner: Runner<Composite>,
	state: Rc<RefCell<State>>,
}

impl App {
	fn change_page(&mut self, direction: i8) {
		let (pc, _) = self.memory.borrow().pc();
		let pc_page = pc.to_le_bytes()[1];
		self.page = match self.page {
			None => Some(pc_page.saturating_add_signed(direction)),
			Some(page) => {
				let page = page.saturating_add_signed(direction);
				if page == pc_page {
					None
				} else {
					Some(page)
				}
			}
		}
	}

	fn exit(&mut self) {
		self.exit = true;
	}

	fn handle_events(&mut self) -> io::Result<()> {
		match event::read()? {
			// it's important to check that the event is a key press event as
			// crossterm also emits key release and repeat events on Windows.
			Event::Key(key_event) if key_event.kind == KeyEventKind::Press => {
				self.handle_key_event(key_event)
			}
			_ => {}
		};
		Ok(())
	}

	fn handle_key_event(&mut self, key_event: KeyEvent) {
		match key_event.code {
			KeyCode::PageUp => self.change_page(1),
			KeyCode::PageDown => self.change_page(-1),
			KeyCode::Char(' ') => {
				self.page = None;
				self.runner.run();
			}
			KeyCode::Char('q') => self.exit(),
			_ => {}
		}
	}

	pub fn new() -> io::Result<Self> {
		let disassembler = Rc::new(RefCell::new(SimpleDisassembler::new(16)));
		let memory = Rc::new(RefCell::new(Memory::new()));
		let state = Rc::new(RefCell::new(State::new()));

		let mut debugger = Composite::new();
		debugger.add(Rc::new(RefCell::new(NestestLog::new("nestest.log")?)));
		debugger.add(state.clone());
		debugger.add(memory.clone());
		debugger.add(disassembler.clone());

		Ok(Self {
			disassembler,
			exit: false,
			memory,
			page: None,
			runner: Runner::with_debugger(debugger),
			state,
		})
	}

	fn render_cpu_view(&mut self, area: Rect, buf: &mut Buffer) {
		let title = Title::from(" CPU View ".bold());
		let block = Block::default()
			.title(title.alignment(Alignment::Center))
			.borders(Borders::ALL)
			.border_set(border::THICK);

		let cycles = self.state.borrow().cycles();
		let registers = self.state.borrow().registers();
		let status = registers.p().as_u8();
		let text = Text::from(format!(
			"PC: {:04X} A: {:02X} X: {:02X} Y: {:02X} SP: {:02X} {}{}{}{}{}{}{}{}    CYC:{}",
			registers.pc(),
			registers.a(),
			registers.x(),
			registers.y(),
			registers.s(),
			if (status & 0x80) == 0x80 { 'N' } else { 'n' },
			if (status & 0x40) == 0x40 { 'V' } else { 'v' },
			if (status & 0x20) == 0x20 { '-' } else { ' ' },
			if (status & 0x10) == 0x10 { 'B' } else { 'b' },
			if (status & 0x08) == 0x08 { 'D' } else { 'd' },
			if (status & 0x04) == 0x04 { 'I' } else { 'i' },
			if (status & 0x02) == 0x02 { 'Z' } else { 'z' },
			if (status & 0x01) == 0x01 { 'C' } else { 'c' },
			cycles
		));

		Paragraph::new(text)
			.left_aligned()
			.block(block)
			.render(area, buf);
	}

	fn render_disassembly_view(&mut self, area: Rect, buf: &mut Buffer) {
		let title = Title::from(" Disassembly View ".bold());
		let block = Block::default()
			.title(title.alignment(Alignment::Center))
			.borders(Borders::ALL)
			.border_set(border::THICK);

		let disassembler = self.disassembler.borrow();
		let text = Text::from(
			disassembler
				.lines()
				.iter()
				.map(|line| line.as_ref().into())
				.collect::<Vec<_>>(),
		);

		Paragraph::new(text)
			.left_aligned()
			.block(block)
			.render(area, buf);
	}

	fn render_frame(&mut self, frame: &mut Frame) {
		frame.render_widget(self, frame.size());
	}

	fn render_memory_view(&mut self, area: Rect, buf: &mut Buffer) {
		let title = Title::from(" Memory View ".bold());
		let instructions = Title::from(Line::from(vec![
			" Previous Page ".into(),
			"<PageDown>".blue().bold(),
			" Next Page ".into(),
			"<PageUp> ".blue().bold(),
		]));
		let block = Block::default()
			.title(title.alignment(Alignment::Center))
			.title(
				instructions
					.alignment(Alignment::Center)
					.position(Position::Bottom),
			)
			.borders(Borders::ALL)
			.border_set(border::THICK);

		let memory = self.memory.borrow();
		let pc = match self.page {
			None => memory.pc(),
			Some(page) => (u16::from_le_bytes([0x00, page]), 0),
		};
		let page = pc.0.to_le_bytes()[1];

		MemoryView::new(memory.page(page), pc)
			.block(block)
			.render(area, buf);
	}

	/// runs the application's main loop until the user quits
	pub fn run(&mut self, terminal: &mut tui::Tui) -> io::Result<()> {
		self.runner.reset();
		while !self.exit {
			terminal.draw(|frame| self.render_frame(frame))?;
			self.handle_events()?;
		}
		Ok(())
	}
}

impl Widget for &mut App {
	fn render(self, area: Rect, buf: &mut Buffer) {
		let title = Title::from(" nes-rs ".bold());
		let instructions = Title::from(Line::from(vec![
			" Advance ".into(),
			"<Space>".blue().bold(),
			" Quit ".into(),
			"<Q> ".blue().bold(),
		]));
		let block = Block::default()
			.title(title.alignment(Alignment::Center))
			.title(
				instructions
					.alignment(Alignment::Center)
					.position(Position::Bottom),
			)
			.borders(Borders::ALL)
			.border_set(border::THICK);
		(&block).render(area, buf);

		let layout = Layout::default()
			.direction(Direction::Vertical)
			.constraints([Constraint::Length(3), Constraint::Fill(1)])
			.split(block.inner(area));
		let sub_layout = Layout::default()
			.direction(Direction::Horizontal)
			.constraints([Constraint::Percentage(50), Constraint::Percentage(50)])
			.split(layout[1]);

		self.render_cpu_view(layout[0], buf);
		self.render_memory_view(sub_layout[0], buf);
		self.render_disassembly_view(sub_layout[1], buf);
	}
}

fn main() -> io::Result<()> {
	let mut terminal = tui::init()?;
	let app_result = App::new()?.run(&mut terminal);
	tui::restore()?;
	app_result
}
