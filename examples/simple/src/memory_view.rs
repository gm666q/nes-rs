use ratatui::prelude::*;
use ratatui::widgets::Block;

pub struct MemoryView<'a> {
	block: Option<Block<'a>>,
	data: [u8; 256],
	pc: (u16, u8),
}

impl MemoryView<'_> {
	fn render_memory_view(self, area: Rect, buf: &mut Buffer) {
		if area.width < 53 || area.height < 16 {
			buf.set_style(area, Style::default().on_red());
			return;
		}

		let page = self.pc.0 & 0xff00;
		let i = self.pc.0 & 0x00ff;
		let range = i..(i + self.pc.1 as u16);

		for y in 0..16 {
			let y_offset = y * 0x10;
			buf.set_string(
				area.x,
				area.y + y,
				format!("{:04X}:", page + y_offset),
				Style::default(),
			);

			for x in 0..16 {
				let x_offset = y_offset + x;
				let styles = if range.contains(&x_offset) {
					Style::default().red().bold()
				} else {
					Style::default()
				};
				buf.set_string(
					area.x + (x * 3) + 5,
					area.y + y,
					&format!(" {:02X}", self.data[x_offset as usize]),
					styles,
				);
			}
		}
	}
}

impl<'a> MemoryView<'a> {
	#[must_use]
	pub fn block(mut self, block: Block<'a>) -> Self {
		self.block = Some(block);
		self
	}

	pub fn new(data: [u8; 256], pc: (u16, u8)) -> Self {
		Self {
			block: None,
			data,
			pc,
		}
	}
}

impl Widget for MemoryView<'_> {
	fn render(self, area: Rect, buf: &mut Buffer) {
		//buf.set_style(area, self.style);
		self.block.render(area, buf);
		let inner = self.block.inner_if_some(area);
		self.render_memory_view(inner, buf);
	}
}
