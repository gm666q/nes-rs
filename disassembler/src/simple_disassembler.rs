use std::collections::HashMap;

use core::{Bus, Cpu, Debugger};

use super::cpu::Instruction;

pub struct SimpleDisassembler {
	labels: HashMap<u16, String>,
	lines: Vec<Box<str>>,
}

impl SimpleDisassembler {
	/*pub fn disassemble_label(&mut self, mut address: u16, label: Option<&'static str>) -> Vec<u16> {
		let result = Vec::new();

		let label = match label {
			None => format!("L{address:04X}"),
			Some(label) => label.to_owned(),
		};

		let instruction = self.read_instruction(address, Some(label));
		address += instruction.bytes() as u16;
		println!("{instruction}");

		for _ in 0..63 {
			let instruction = self.read_instruction(address, None);
			address += instruction.bytes() as u16;
			println!("{instruction}");
		}

		result
	}*/

	fn disassemble_lines(&mut self, bus: &mut Bus) {
		let mut address = bus.cpu_registers().pc();

		for line in self.lines.iter_mut() {
			let instruction =
				Self::read_instruction(bus, address, self.labels.get(&address).cloned());
			address += instruction.bytes() as u16;
			*line = instruction.to_string().into_boxed_str();
		}
	}

	/*pub fn disassemble_rom(&mut self) {
		//let mut pc = self.read_u16(Cpu::RESET_ADDRESS);
		let mut pc = 0xC000;
		self.disassemble_label(pc, Some("RESET"));
	}*/

	pub fn lines(&self) -> &[Box<str>] {
		&self.lines
	}

	pub fn new(look_ahead: u8) -> Self {
		Self {
			labels: HashMap::new(),
			lines: vec![Box::default(); look_ahead as usize],
		}
	}

	fn read_instruction(bus: &mut Bus, address: u16, label: Option<String>) -> Instruction {
		Instruction::new(
			address,
			[
				Self::read_u8(bus, address.wrapping_add(0)),
				Self::read_u8(bus, address.wrapping_add(1)),
				Self::read_u8(bus, address.wrapping_add(2)),
			],
			label,
		)
	}

	fn read_u8(bus: &mut Bus, address: u16) -> u8 {
		bus.cpu_read(address, true)
	}

	fn read_u16(bus: &mut Bus, address: u16) -> u16 {
		u16::from_le_bytes([
			bus.cpu_read(address.wrapping_add(0), true),
			bus.cpu_read(address.wrapping_add(1), true),
		])
	}
}

impl Debugger for SimpleDisassembler {
	fn init(&mut self, bus: &mut Bus) {
		self.labels.insert(
			Self::read_u16(bus, Cpu::RESET_ADDRESS),
			String::from("RESET"),
		);
	}

	fn post_instruction(&mut self, bus: &mut Bus) {
		self.disassemble_lines(bus);
	}

	fn post_reset(&mut self, bus: &mut Bus) {
		self.disassemble_lines(bus);
	}

	fn post_tick(&mut self, _: &mut Bus) {}

	fn pre_instruction(&mut self, _: &mut Bus) {}

	fn pre_reset(&mut self, _: &mut Bus) {}

	fn pre_tick(&mut self, _: &mut Bus) {}
}
