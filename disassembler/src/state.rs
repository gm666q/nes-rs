use nes_core::{cpu, Bus, Debugger};

pub struct State {
	cycles: u64,
	registers: cpu::Registers,
}

impl State {
	pub const fn cycles(&self) -> u64 {
		self.cycles
	}

	pub const fn new() -> Self {
		Self {
			cycles: 7,
			registers: cpu::Registers::new(),
		}
	}

	pub const fn registers(&self) -> cpu::Registers {
		self.registers
	}
}

impl Debugger for State {
	fn init(&mut self, _: &mut Bus) {}

	fn post_instruction(&mut self, bus: &mut Bus) {
		self.registers = bus.cpu_registers();
	}

	fn post_reset(&mut self, bus: &mut Bus) {
		self.registers = bus.cpu_registers();
	}

	fn post_tick(&mut self, _: &mut Bus) {}

	fn pre_instruction(&mut self, _: &mut Bus) {}

	fn pre_reset(&mut self, _: &mut Bus) {}

	fn pre_tick(&mut self, _: &mut Bus) {
		self.cycles += 1;
	}
}
