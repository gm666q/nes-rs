use core::{cpu::OperationCode, Bus, Debugger};

pub struct Memory {
	pages: [[u8; 256]; 256],
	pc: (u16, u8),
}

impl Memory {
	pub const fn new() -> Self {
		Self {
			pages: [[0; 256]; 256],
			pc: (0, 0),
		}
	}

	pub fn page(&self, i: u8) -> [u8; 256] {
		self.pages[i as usize]
	}

	pub fn pc(&self) -> (u16, u8) {
		self.pc
	}

	#[inline]
	fn take_snapshot(&mut self, bus: &mut Bus) {
		for i in 0..=0xff {
			for j in 0..=0xff {
				self.pages[i as usize][j as usize] = bus.cpu_read(u16::from_le_bytes([j, i]), true);
			}
		}

		let pc = bus.cpu_registers().pc();
		let [lo, hi] = pc.to_le_bytes();
		self.pc = (
			pc,
			OperationCode::LOOKUP[self.pages[hi as usize][lo as usize] as usize]
				.1
				.bytes(),
		);
	}
}

impl Debugger for Memory {
	fn init(&mut self, _: &mut Bus) {}

	fn post_instruction(&mut self, bus: &mut Bus) {
		self.take_snapshot(bus);
	}

	fn post_reset(&mut self, bus: &mut Bus) {
		self.take_snapshot(bus);
	}

	fn post_tick(&mut self, _: &mut Bus) {}

	fn pre_instruction(&mut self, _: &mut Bus) {}

	fn pre_reset(&mut self, _: &mut Bus) {}

	fn pre_tick(&mut self, _: &mut Bus) {}
}
