use std::{
	fs::File,
	io::{self, Write},
	path::Path,
};

use core::{Bus, Debugger};

use super::Instruction;

pub struct NestestLog {
	cycles: u64,
	file: File,
}

impl NestestLog {
	pub fn new<P: AsRef<Path>>(path: P) -> io::Result<Self> {
		let file = File::create(path)?;

		Ok(Self { cycles: 7, file })
	}

	fn read_instruction(bus: &mut Bus, address: u16) -> Instruction {
		let bytes = [
			Self::read_u8(bus, address.wrapping_add(0)),
			Self::read_u8(bus, address.wrapping_add(1)),
			Self::read_u8(bus, address.wrapping_add(2)),
		];
		Instruction::new(bus, address, bytes)
	}

	fn read_u8(bus: &mut Bus, address: u16) -> u8 {
		bus.cpu_read(address, true)
	}

	fn read_u16(bus: &mut Bus, address: u16) -> u16 {
		u16::from_le_bytes([
			bus.cpu_read(address.wrapping_add(0), true),
			bus.cpu_read(address.wrapping_add(1), true),
		])
	}
}

impl Debugger for NestestLog {
	fn init(&mut self, _: &mut Bus) {}

	fn post_instruction(&mut self, _: &mut Bus) {}

	fn post_reset(&mut self, bus: &mut Bus) {
		bus.cpu_registers_mut().load_pc(0xc000);
	}

	fn post_tick(&mut self, _: &mut Bus) {}

	fn pre_instruction(&mut self, bus: &mut Bus) {
		let registers = bus.cpu_registers();
		let instruction = Self::read_instruction(bus, registers.pc());

		writeln!(
			self.file,
			"{instruction} A:{:02X} X:{:02X} Y:{:02X} P:{:02X} SP:{:02X} PPU:{:>3},{:>3} CYC:{}",
			registers.a(),
			registers.x(),
			registers.y(),
			registers.p().as_u8(),
			registers.s(),
			0,
			0,
			self.cycles
		)
		.unwrap();
	}

	fn pre_reset(&mut self, _: &mut Bus) {}

	fn pre_tick(&mut self, _: &mut Bus) {
		self.cycles += 1;
	}
}
