use std::fmt;

use core::{
	cpu::{self, AddressingMode, OperationCode},
	Bus,
};

use super::Operand;

#[derive(Clone)]
pub struct Instruction {
	address: u16,
	bytes: [u8; 3],
	operand: Operand,
	operation_code: OperationCode,
}

impl Instruction {
	#[must_use]
	fn addressing_mode_string(&self) -> String {
		use {Operand::*, OperationCode::*};

		match self.operand {
			ABS(value, data) => match self.operation_code {
				JMP | JSR => format!("${value:04X}"),
				_ => format!("${value:04X} = {data:02X}"),
			},
			ABX(value) => format!("${value:04X},X"),
			ABY(value) => format!("${value:04X},Y"),
			ACC => format!("A"),
			IDA(value) => format!("(${value:04X})"),
			IDX(value, address0, address1, data) => {
				format!("(${value:02X},X) @ {address0:02X} = {address1:04X} = {data:02X}")
			}
			IDY(value, address0, address1, data) => {
				format!("(${value:02X}),Y = {address0:04X} @ {address1:04X} = {data:02X}")
			}
			IMM(value) => format!("#${value:02X}"),
			IMP => format!(""),
			REL(value) => {
				let value = self
					.address
					.wrapping_add(2)
					.wrapping_add_signed(i16::from_le_bytes([
						value,
						if (value & 0x80) == 0x80 { 0xff } else { 0x00 },
					]));
				format!("${value:04X}")
			}
			ZPG(value, data) => match self.operation_code {
				JMP | JSR => format!("${value:02X}"),
				_ => format!("${value:02X} = {data:02X}"),
			},
			ZPX(value) => format!("${value:02X},X"),
			ZPY(value) => format!("${value:02X},Y"),
		}
	}

	#[must_use]
	fn bytes_string(&self) -> String {
		use Operand::*;

		match self.operand {
			ABS(_, _) => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			ABX(_) => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			ABY(_) => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			ACC => format!("{:02X}", self.bytes[0]),
			IDA(_) => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			IDX(_, _, _, _) => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			IDY(_, _, _, _) => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			IMM(_) => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			IMP => format!("{:02X}", self.bytes[0]),
			REL(_) => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			ZPG(_, _) => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			ZPX(_) => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			ZPY(_) => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
		}
	}

	pub fn new(bus: &mut Bus, address: u16, bytes: [u8; 3]) -> Self {
		let (operation_code, addressing_mode, cycles) = OperationCode::LOOKUP[bytes[0] as usize];
		let operand = match addressing_mode {
			AddressingMode::ABS => {
				let address = u16::from_le_bytes([bytes[1], bytes[2]]);
				Operand::ABS(address, bus.cpu_read(address, true))
			}
			AddressingMode::ABX => Operand::ABX(u16::from_le_bytes([bytes[1], bytes[2]])),
			AddressingMode::ABY => Operand::ABY(u16::from_le_bytes([bytes[1], bytes[2]])),
			AddressingMode::ACC => Operand::ACC,
			AddressingMode::IDA => Operand::IDA(u16::from_le_bytes([bytes[1], bytes[2]])),
			AddressingMode::IDX => {
				let address0 = bytes[1].wrapping_add(bus.cpu_registers().x());
				let address0_full = u16::from_le_bytes([address0, 0x00]);
				let address1 = u16::from_le_bytes([
					bus.cpu_read(address0_full.wrapping_add(0), true),
					bus.cpu_read(address0_full.wrapping_add(1), true),
				]);

				Operand::IDX(
					u8::from_le_bytes([bytes[1]]),
					address0,
					address1,
					bus.cpu_read(address1, true),
				)
			}
			AddressingMode::IDY => {
				let value = u16::from_le_bytes([bytes[1], 0x00]);

				let address0 = u16::from_le_bytes([
					bus.cpu_read(value.wrapping_add(0), true),
					bus.cpu_read(value.wrapping_add(1), true),
				]);
				let address1 = address0.wrapping_add(bus.cpu_registers().y() as u16);

				Operand::IDY(
					u8::from_le_bytes([bytes[1]]),
					address0,
					address1,
					bus.cpu_read(address1, true),
				)
			}
			AddressingMode::IMM => Operand::IMM(u8::from_le_bytes([bytes[1]])),
			AddressingMode::IMP => Operand::IMP,
			AddressingMode::REL => Operand::REL(u8::from_le_bytes([bytes[1]])),
			AddressingMode::ZPG => Operand::ZPG(
				u8::from_le_bytes([bytes[1]]),
				bus.cpu_read(u16::from_le_bytes([bytes[1], 0x00]), false),
			),
			AddressingMode::ZPX => Operand::ZPX(u8::from_le_bytes([bytes[1]])),
			AddressingMode::ZPY => Operand::ZPY(u8::from_le_bytes([bytes[1]])),
		};
		Self {
			address,
			bytes,
			operand,
			operation_code,
		}
	}

	#[inline]
	#[must_use]
	const fn operation_code_string(&self) -> &'static str {
		use OperationCode::*;

		match self.operation_code {
			ADC => "ADC",
			ANC => "ANC",
			AND => "AND",
			ARR => "ARR",
			ASL => "ASL",
			ASR => "ASR",
			BCC => "BCC",
			BCS => "BCS",
			BEQ => "BEQ",
			BIT => "BIT",
			BMI => "BMI",
			BNE => "BNE",
			BPL => "BPL",
			BRK => "BRK",
			BVC => "BVC",
			BVS => "BVS",
			CLC => "CLC",
			CLD => "CLD",
			CLI => "CLI",
			CLV => "CLV",
			CMP => "CMP",
			CPX => "CPX",
			CPY => "CPY",
			DCP => "DCP",
			DEC => "DEC",
			DEX => "DEX",
			DEY => "DEY",
			EOR => "EOR",
			INC => "INC",
			INX => "INX",
			INY => "INY",
			ISC => "ISC",
			JAM => "JAM",
			JMP => "JMP",
			JSR => "JSR",
			LAS => "LAS",
			LAX => "LAX",
			LDA => "LDA",
			LDX => "LDX",
			LDY => "LDY",
			LSR => "LSR",
			NOP => "NOP",
			ORA => "ORA",
			PHA => "PHA",
			PHP => "PHP",
			PLA => "PLA",
			PLP => "PLP",
			RLA => "RLA",
			ROL => "ROL",
			ROR => "ROR",
			RRA => "RRA",
			RTI => "RTI",
			RTS => "RTS",
			SAX => "SAX",
			SBC => "SBC",
			SBX => "SBX",
			SEC => "SEC",
			SED => "SED",
			SEI => "SEI",
			SHA => "SHA",
			SHS => "SHS",
			SHX => "SHX",
			SHY => "SHY",
			SLO => "SLO",
			SRE => "SRE",
			STA => "STA",
			STX => "STX",
			STY => "STY",
			TAX => "TAX",
			TAY => "TAY",
			TSX => "TSX",
			TXA => "TXA",
			TXS => "TXS",
			TYA => "TYA",
			XAA => "XAA",
		}
	}
}

impl fmt::Display for Instruction {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"{:04X}  {:<8}  {} {:<27}",
			self.address,
			self.bytes_string(),
			self.operation_code_string(),
			self.addressing_mode_string()
		)
	}
}
