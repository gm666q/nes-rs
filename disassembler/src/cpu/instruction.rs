use std::fmt;

use core::cpu::{self, AddressingMode, Operand, OperationCode};

#[derive(Clone)]
pub struct Instruction {
	address: u16,
	bytes: [u8; 3],
	inner: cpu::Instruction,
	label: Option<String>,
}

impl Instruction {
	#[must_use]
	fn addressing_mode_string(&self) -> String {
		use Operand::*;

		match self.inner.operand() {
			ABS(value) => format!("${value:04X}"),
			ABX(value) => format!("${value:04X},X"),
			ABY(value) => format!("${value:04X},Y"),
			ACC => format!("A"),
			IDA(value) => format!("(${value:04X})"),
			IDX(value) => format!("(${value:02X},X)"),
			IDY(value) => format!("(${value:02X}),Y"),
			IMM(value) => format!("#${value:02X}"),
			IMP => format!(""),
			REL(value) => format!("${value:02X}"),
			ZPG(value) => format!("${value:02X}"),
			ZPX(value) => format!("${value:02X},X"),
			ZPY(value) => format!("${value:02X},Y"),
		}
	}

	#[inline]
	#[must_use]
	pub fn bytes(&self) -> u8 {
		self.inner.addressing_mode().bytes()
	}

	#[must_use]
	fn bytes_string(&self) -> String {
		use AddressingMode::*;

		match self.inner.addressing_mode() {
			ABS => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			ABX => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			ABY => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			ACC => format!("{:02X}", self.bytes[0]),
			IDA => format!(
				"{:02X} {:02X} {:02X}",
				self.bytes[0], self.bytes[1], self.bytes[2]
			),
			IDX => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			IDY => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			IMM => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			IMP => format!("{:02X}", self.bytes[0]),
			REL => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			ZPG => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			ZPX => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
			ZPY => format!("{:02X} {:02X}", self.bytes[0], self.bytes[1]),
		}
	}

	#[inline]
	#[must_use]
	pub const fn inner(&self) -> &cpu::Instruction {
		&self.inner
	}

	pub const fn new(address: u16, bytes: [u8; 3], label: Option<String>) -> Self {
		let (operation_code, addressing_mode, cycles) = OperationCode::LOOKUP[bytes[0] as usize];
		let operand = match addressing_mode {
			AddressingMode::ABS => Operand::ABS(u16::from_le_bytes([bytes[1], bytes[2]])),
			AddressingMode::ABX => Operand::ABX(u16::from_le_bytes([bytes[1], bytes[2]])),
			AddressingMode::ABY => Operand::ABY(u16::from_le_bytes([bytes[1], bytes[2]])),
			AddressingMode::ACC => Operand::ACC,
			AddressingMode::IDA => Operand::IDA(u16::from_le_bytes([bytes[1], bytes[2]])),
			AddressingMode::IDX => Operand::IDX(u8::from_le_bytes([bytes[1]])),
			AddressingMode::IDY => Operand::IDY(u8::from_le_bytes([bytes[1]])),
			AddressingMode::IMM => Operand::IMM(u8::from_le_bytes([bytes[1]])),
			AddressingMode::IMP => Operand::IMP,
			AddressingMode::REL => Operand::REL(u8::from_le_bytes([bytes[1]])),
			AddressingMode::ZPG => Operand::ZPG(u8::from_le_bytes([bytes[1]])),
			AddressingMode::ZPX => Operand::ZPX(u8::from_le_bytes([bytes[1]])),
			AddressingMode::ZPY => Operand::ZPY(u8::from_le_bytes([bytes[1]])),
		};
		Self {
			address,
			bytes,
			inner: cpu::Instruction::new(cycles, operand, operation_code),
			label,
		}
	}

	#[inline]
	#[must_use]
	const fn operation_code_string(&self) -> &'static str {
		use OperationCode::*;

		match self.inner.operation_code() {
			ADC => "ADC",
			ANC => "ANC",
			AND => "AND",
			ARR => "ARR",
			ASL => "ASL",
			ASR => "ASR",
			BCC => "BCC",
			BCS => "BCS",
			BEQ => "BEQ",
			BIT => "BIT",
			BMI => "BMI",
			BNE => "BNE",
			BPL => "BPL",
			BRK => "BRK",
			BVC => "BVC",
			BVS => "BVS",
			CLC => "CLC",
			CLD => "CLD",
			CLI => "CLI",
			CLV => "CLV",
			CMP => "CMP",
			CPX => "CPX",
			CPY => "CPY",
			DCP => "DCP",
			DEC => "DEC",
			DEX => "DEX",
			DEY => "DEY",
			EOR => "EOR",
			INC => "INC",
			INX => "INX",
			INY => "INY",
			ISC => "ISC",
			JAM => "JAM",
			JMP => "JMP",
			JSR => "JSR",
			LAS => "LAS",
			LAX => "LAX",
			LDA => "LDA",
			LDX => "LDX",
			LDY => "LDY",
			LSR => "LSR",
			NOP => "NOP",
			ORA => "ORA",
			PHA => "PHA",
			PHP => "PHP",
			PLA => "PLA",
			PLP => "PLP",
			RLA => "RLA",
			ROL => "ROL",
			ROR => "ROR",
			RRA => "RRA",
			RTI => "RTI",
			RTS => "RTS",
			SAX => "SAX",
			SBC => "SBC",
			SBX => "SBX",
			SEC => "SEC",
			SED => "SED",
			SEI => "SEI",
			SHA => "SHA",
			SHS => "SHS",
			SHX => "SHX",
			SHY => "SHY",
			SLO => "SLO",
			SRE => "SRE",
			STA => "STA",
			STX => "STX",
			STY => "STY",
			TAX => "TAX",
			TAY => "TAY",
			TSX => "TSX",
			TXA => "TXA",
			TXS => "TXS",
			TYA => "TYA",
			XAA => "XAA",
		}
	}
}

/*impl From<cpu::Instruction> for Instruction {
	fn from(value: cpu::Instruction) -> Self {
		Self(value)
	}
}*/

impl fmt::Display for Instruction {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"{:04X}   {:<8}   {:<10}{} {}",
			self.address,
			self.bytes_string(),
			self.label.as_deref().unwrap_or_default(),
			self.operation_code_string(),
			self.addressing_mode_string()
		)
	}
}
