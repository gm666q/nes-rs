use std::{marker::PhantomPinned, pin::Pin, ptr::NonNull};

use crate::{
	cpu::{self, Cpu},
	Ppu, Ram,
};

pub struct Bus {
	cpu: NonNull<Cpu<'static>>,
	last_read: u8,
	ppu: NonNull<Ppu<'static>>,
	ram: Ram,
	_pinned: PhantomPinned,
}

impl Bus {
	pub fn cpu_read(&mut self, address: u16, synthetic: bool) -> u8 {
		self.last_read = match address {
			0x0000..=0x1fff => {
				//eprintln!("RAM Read");
				self.ram.cpu_read(address, synthetic)
			}
			0x2000..=0x3fff => {
				//eprintln!("PPU Registers Read");
				unsafe { self.ppu.as_mut() }.cpu_read(address, synthetic)
			}
			0x4000..=0x4017 => {
				//eprintln!("APU and I/O Registers Read");
				0
			}
			0x4018..=0x401f => {
				//eprintln!("APU and I/O Test Mode Read");
				0
			}
			0x4020..=0xffff => {
				//eprintln!("Unmapped Read");
				0
			},
		};

		self.last_read
	}

	pub fn cpu_registers(&self) -> cpu::Registers {
		unsafe { self.cpu.as_ref() }.registers()
	}

	pub fn cpu_registers_mut(&mut self) -> &mut cpu::Registers {
		unsafe { self.cpu.as_mut() }.registers_mut()
	}

	pub fn cpu_write(&mut self, address: u16, value: u8) {
		match address {
			0x0000..=0x1fff => {
				//eprintln!("RAM Write");
				self.ram.cpu_write(address, value);
			}
			0x2000..=0x3fff => {
				//eprintln!("PPU Registers Write");
				unsafe { self.ppu.as_mut() }.cpu_write(address, value);
			}
			_ => {}
		}
	}

	#[inline]
	pub fn is_halted(&self) -> bool {
		unsafe { self.cpu.as_ref() }.is_halted()
	}

	pub fn new() -> Pin<Box<Self>> {
		let this = Self {
			cpu: NonNull::dangling(),
			last_read: 0,
			ppu: NonNull::dangling(),
			ram: Ram::zeroed(),
			_pinned: PhantomPinned,
		};

		let mut this = Box::pin(this);
		let this_ptr: *mut Self = unsafe { this.as_mut().get_unchecked_mut() };

		unsafe {
			(*this_ptr).cpu =
				NonNull::new(Box::into_raw(Box::new(Cpu::new(&mut *this_ptr)))).unwrap();
			(*this_ptr).ppu =
				NonNull::new(Box::into_raw(Box::new(Ppu::new(&mut *this_ptr)))).unwrap();
		}

		this
	}

	pub fn reset(self: Pin<&mut Self>) {
		let this_ref = unsafe { self.get_unchecked_mut() };

		unsafe { this_ref.cpu.as_mut() }.reset();
		unsafe { this_ref.ppu.as_mut() }.reset();
	}

	pub fn tick(self: Pin<&mut Self>) -> u8 {
		let this_ref = unsafe { self.get_unchecked_mut() };

		unsafe { this_ref.cpu.as_mut() }.tick()
		//unsafe { this_ref.ppu.as_mut() }.tick()
	}
}

impl Drop for Bus {
	fn drop(&mut self) {
		let _cpu = unsafe { Box::from_raw(self.cpu.as_ptr()) };
		self.cpu = NonNull::dangling();
		let _ppu = unsafe { Box::from_raw(self.ppu.as_ptr()) };
		self.ppu = NonNull::dangling();
	}
}
