use bitflags::bitflags;

bitflags! {
	#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
	#[repr(transparent)]
	pub struct Status: u8 {
		const C = 0x01;
		const Z = 0x02;
		const I = 0x04;
		const D = 0x08;
		const B = 0x10;
		const V = 0x40;
		const N = 0x80;

		//const _ = !0;
	}
}

impl Status {
	#[inline]
	pub const fn as_u8(&self) -> u8 {
		self.bits() | 0x20
	}
}
