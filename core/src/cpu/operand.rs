#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum Operand {
	ABS(u16),
	ABX(u16),
	ABY(u16),
	ACC,
	IDA(u16),
	IDX(u8),
	IDY(u8),
	IMM(u8),
	IMP,
	REL(u8),
	ZPG(u8),
	ZPX(u8),
	ZPY(u8),
}
