use crate::{ops::PreSubOne, Bus};

use super::{AddressingMode, Instruction, Operand, OperationCode, Registers, Status};

struct OperandResult {
	address: Option<u16>,
	is_page_crossed: bool,
	value: Option<u8>,
}

impl OperandResult {
	#[inline]
	#[must_use]
	pub const fn new(address: Option<u16>, is_page_crossed: bool, value: Option<u8>) -> Self {
		Self {
			address,
			is_page_crossed,
			value,
		}
	}
}

pub struct Cpu<'a> {
	bus: &'a mut Bus,
	cycles: u8,
	halt: bool,
	registers: Registers,
}

impl Cpu<'_> {
	pub const IRQ_BRK_ADDRESS: u16 = 0xFFFE;
	pub const NMI_ADDRESS: u16 = 0xFFFA;
	pub const RESET_ADDRESS: u16 = 0xFFFC;
}

impl<'a> Cpu<'a> {
	fn execute_operand(&mut self, operand: Operand) -> OperandResult {
		use Operand::*;

		match operand {
			ABS(value) => OperandResult::new(Some(value), false, None),
			ABX(value) => {
				let [lo, hi] = value.to_le_bytes();
				let (lo, c) = lo.overflowing_add(self.registers.x());

				if c {
					let _ = self.read_u8(u16::from_le_bytes([lo, hi]));
				}
				let address = u16::from_le_bytes([lo, hi.wrapping_add(c.into())]);

				OperandResult::new(Some(address), c, None)
			}
			ABY(value) => {
				let [lo, hi] = value.to_le_bytes();
				let (lo, c) = lo.overflowing_add(self.registers.y());

				if c {
					let _ = self.read_u8(u16::from_le_bytes([lo, hi]));
				}
				let address = u16::from_le_bytes([lo, hi.wrapping_add(c.into())]);

				OperandResult::new(Some(address), c, None)
			}
			ACC => OperandResult::new(None, false, Some(self.registers.a())),
			IDA(value) => OperandResult::new(Some(self.read_u16(value)), false, None),
			IDX(value) => {
				let _ = self.read_u8(u16::from_le_bytes([value, 0x00]));
				let address = self.read_u16(u16::from_le_bytes([
					value.wrapping_add(self.registers.x()),
					0x00,
				]));
				OperandResult::new(Some(address), false, None)
			}
			IDY(value) => {
				let [lo, hi] = self
					.read_u16(u16::from_le_bytes([value, 0x00]))
					.to_le_bytes();
				let (lo, c) = lo.overflowing_add(self.registers.y());

				if c {
					let _ = self.read_u8(u16::from_le_bytes([lo, hi]));
				}
				let address = u16::from_le_bytes([lo, hi.wrapping_add(c.into())]);

				OperandResult::new(Some(address), c, None)
			}
			IMM(value) => OperandResult::new(None, false, Some(value)),
			IMP => OperandResult::new(None, false, None),
			REL(value) => {
				// TODO: Check
				let address = self.registers.offset_pc(value);
				OperandResult::new(
					Some(address),
					self.registers.page() != (address & 0xff00),
					None,
				)
			}
			ZPG(value) => {
				let address = u16::from_le_bytes([value, 0x00]);
				OperandResult::new(Some(address), false, None)
			}
			ZPX(value) => {
				let _ = self.read_u8(u16::from_le_bytes([value, 0x00]));
				let address = u16::from_le_bytes([value.wrapping_add(self.registers.x()), 0x00]);
				OperandResult::new(Some(address), false, None)
			}
			ZPY(value) => {
				let _ = self.read_u8(u16::from_le_bytes([value, 0x00]));
				let address = u16::from_le_bytes([value.wrapping_add(self.registers.y()), 0x00]);
				OperandResult::new(Some(address), false, None)
			}
		}
	}

	fn execute_operation_code(
		&mut self,
		operand_result: OperandResult,
		operation_code: OperationCode,
	) -> u8 {
		use OperationCode::*;

		match operation_code {
			ADC => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};
				//let (result, carry) = self.registers.a().carrying_add(value, self.registers.is_flag_set(Status::C)); // TODO: Enable when stabilized
				let (a, b) = self.registers.a().overflowing_add(value);
				let (c, d) = a.overflowing_add(self.registers.is_flag_set(Status::C) as u8);
				let (result, carry) = (c, b || d);

				self.registers.set_flag(
					Status::V,
					((value ^ result) & (self.registers.a() ^ result)) & 0x80 != 0x00,
				);
				self.registers.set_flag(Status::C, carry);
				self.registers.load_a(result);

				operand_result.is_page_crossed.into()
			}
			AND => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};

				self.registers.load_a(self.registers.a() & value);

				operand_result.is_page_crossed.into()
			}
			ASL => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let (address, value) = match operand_result.value {
					None => {
						let address = operand_result.address.unwrap();
						(Some(address), self.read_u8(address))
					}
					Some(value) => (None, value),
				};

				self.registers.set_flag(Status::C, (value & 0x80) == 0x80);
				let value = value << 1;
				if let Some(address) = address {
					self.write_u8(address, value);
					self.registers.set_flag(Status::N, (value & 0x80) == 0x80);
					self.registers.set_flag(Status::Z, value == 0x00);
				} else {
					self.registers.load_a(value);
				}

				0
			}
			BCC => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::C, false, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			BCS => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::C, true, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			BEQ => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::Z, true, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			BIT => {
				assert!(operand_result.address.is_some());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let value = self.read_u8(operand_result.address.unwrap());

				self.registers.set_flag(Status::N, (value & 0x80) == 0x80);
				self.registers.set_flag(Status::V, (value & 0x40) == 0x40);
				self.registers
					.set_flag(Status::Z, (self.registers.a() & value) == 0x00);

				0
			}
			BMI => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::N, true, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			BNE => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::Z, false, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			BPL => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::N, false, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			BRK => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.write_u16_at_s(self.registers.pc().wrapping_add(1));
				self.write_u8_at_s(self.registers.p().union(Status::B).as_u8());

				let address = self.read_u16(Self::IRQ_BRK_ADDRESS);
				self.registers.load_pc(address);

				0
			}
			BVC => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::V, false, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			BVS => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				match (
					self.registers
						.branch(Status::V, true, operand_result.address.unwrap()),
					operand_result.is_page_crossed,
				) {
					(false, _) => 0,
					(true, false) => 1,
					(true, true) => 2,
				}
			}
			CLC => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.set_flag(Status::C, false);

				0
			}
			CLD => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.set_flag(Status::D, false);

				0
			}
			CLI => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.set_flag(Status::I, false);
				0
			}
			CLV => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.set_flag(Status::V, false);
				0
			}
			CMP => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};
				let result = self.registers.a().wrapping_sub(value);

				self.registers.set_flag(Status::N, (result & 0x80) == 0x80);
				self.registers.set_flag(Status::Z, result == 0x00);
				self.registers
					.set_flag(Status::C, value <= self.registers.a());

				operand_result.is_page_crossed.into()
			}
			CPX => {
				//assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};
				let result = self.registers.x().wrapping_sub(value);

				self.registers.set_flag(Status::N, (result & 0x80) == 0x80);
				self.registers.set_flag(Status::Z, result == 0x00);
				self.registers
					.set_flag(Status::C, value <= self.registers.x());

				operand_result.is_page_crossed.into()
			}
			CPY => {
				//assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};
				let result = self.registers.y().wrapping_sub(value);

				self.registers.set_flag(Status::N, (result & 0x80) == 0x80);
				self.registers.set_flag(Status::Z, result == 0x00);
				self.registers
					.set_flag(Status::C, value <= self.registers.y());

				operand_result.is_page_crossed.into()
			}
			DEC => {
				assert!(operand_result.address.is_some());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let address = operand_result.address.unwrap();
				let value = self.read_u8(address);

				let result = value.wrapping_sub(1);
				self.write_u8(address, result);
				self.registers.set_flag(Status::N, (result & 0x80) == 0x80);
				self.registers.set_flag(Status::Z, result == 0x00);

				0
			}
			DEX => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_x(self.registers.x().wrapping_sub(1));

				0
			}
			DEY => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_y(self.registers.y().wrapping_sub(1));

				0
			}
			EOR => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};

				self.registers.load_a(self.registers.a() ^ value);

				operand_result.is_page_crossed.into()
			}
			INC => {
				assert!(operand_result.address.is_some());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let address = operand_result.address.unwrap();
				let value = self.read_u8(address);

				let result = value.wrapping_add(1);
				self.write_u8(address, result);
				self.registers.set_flag(Status::N, (result & 0x80) == 0x80);
				self.registers.set_flag(Status::Z, result == 0x00);

				0
			}
			INX => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_x(self.registers.x().wrapping_add(1));

				0
			}
			INY => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_y(self.registers.y().wrapping_add(1));

				0
			}
			JAM => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.halt = true;

				0
			}
			JMP => {
				assert!(operand_result.address.is_some());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_pc(operand_result.address.unwrap());

				0
			}
			JSR => {
				assert!(operand_result.address.is_some());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				// Subtract one to account for a PC increment in address resolution
				self.write_u16_at_s(self.registers.pc().wrapping_sub(1));
				self.registers.load_pc(operand_result.address.unwrap());

				0
			}
			LDA => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};

				self.registers.load_a(value);

				operand_result.is_page_crossed.into()
			}
			LDX => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};

				self.registers.load_x(value);

				operand_result.is_page_crossed.into()
			}
			LDY => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};

				self.registers.load_y(value);

				operand_result.is_page_crossed.into()
			}
			LSR => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let (address, value) = match operand_result.value {
					None => {
						let address = operand_result.address.unwrap();
						(Some(address), self.read_u8(address))
					}
					Some(value) => (None, value),
				};

				self.registers.set_flag(Status::C, (value & 0x01) == 0x01);
				let value = value >> 1;
				if let Some(address) = address {
					self.write_u8(address, value);
					self.registers.set_flag(Status::N, (value & 0x80) == 0x80);
					self.registers.set_flag(Status::Z, value == 0x00);
				} else {
					self.registers.load_a(value);
				}

				0
			}
			NOP => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				operand_result.is_page_crossed.into()
			}
			ORA => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};

				self.registers.load_a(self.registers.a() | value);

				operand_result.is_page_crossed.into()
			}
			PHA => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.write_u8_at_s(self.registers.a());

				0
			}
			PHP => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.write_u8_at_s(self.registers.p().union(Status::B).as_u8());

				0
			}
			PLA => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let value = self.read_u8_at_s();

				self.registers.load_a(value);

				0
			}
			PLP => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let value = self.read_u8_at_s();

				self.registers.load_p(value);

				0
			}
			ROL => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let carry = self.registers.is_flag_set(Status::C);
				let (address, value) = match operand_result.value {
					None => {
						let address = operand_result.address.unwrap();
						(Some(address), self.read_u8(address))
					}
					Some(value) => (None, value),
				};

				self.registers.set_flag(Status::C, (value & 0x80) == 0x80);
				let value = if carry {
					(value << 1) | 0x01
				} else {
					(value << 1) & 0xfe
				};
				if let Some(address) = address {
					self.write_u8(address, value);
					self.registers.set_flag(Status::N, (value & 0x80) == 0x80);
				} else {
					self.registers.load_a(value);
				}

				0
			}
			ROR => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let carry = self.registers.is_flag_set(Status::C);
				let (address, value) = match operand_result.value {
					None => {
						let address = operand_result.address.unwrap();
						(Some(address), self.read_u8(address))
					}
					Some(value) => (None, value),
				};

				self.registers.set_flag(Status::C, (value & 0x01) == 0x01);
				let value = if carry {
					(value >> 1) | 0x80
				} else {
					(value >> 1) & 0x7f
				};
				if let Some(address) = address {
					self.write_u8(address, value);
					self.registers.set_flag(Status::N, (value & 0x80) == 0x80);
				} else {
					self.registers.load_a(value);
				}

				0
			}
			RTI => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let p = self.read_u8_at_s();
				let pc = self.read_u16_at_s();

				self.registers.load_p(p);
				self.registers.load_pc(pc);

				0
			}
			RTS => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let value = self.read_u16_at_s().wrapping_add(1);

				self.registers.load_pc(value);

				0
			}
			SBC => {
				//assert!(operand_result.address.is_none());
				//assert!(operand_result.is_page_crossed);
				//assert!(operand_result.value.is_none());

				let value = match operand_result.value {
					None => self.read_u8(operand_result.address.unwrap()),
					Some(value) => value,
				};
				let value = (value as i8).wrapping_neg().wrapping_sub(1) as u8;
				//let (result, carry) = self.registers.a().carrying_add(value, self.registers.is_flag_set(Status::C)); // TODO: Enable when stabilized
				let (a, b) = self.registers.a().overflowing_add(value);
				let (c, d) = a.overflowing_add(self.registers.is_flag_set(Status::C) as u8);
				let (result, carry) = (c, b || d);

				self.registers.set_flag(
					Status::V,
					((value ^ result) & (self.registers.a() ^ result)) & 0x80 != 0x00,
				);
				self.registers.set_flag(Status::C, carry);
				self.registers.load_a(result);

				operand_result.is_page_crossed.into()
			}
			SEC => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.set_flag(Status::C, true);

				0
			}
			SED => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.set_flag(Status::D, true);

				0
			}
			SEI => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.set_flag(Status::I, true);

				0
			}
			SRE => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				let address = operand_result.address.unwrap();
				let value = self.read_u8(address);

				self.write_u8(address, value);
				self.registers.set_flag(Status::C, (value & 0x01) == 0x01);

				let value = value >> 1;
				self.write_u8(address, value);
				self.registers.load_a(self.registers.a() ^ value);

				0
			}
			STA => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.write_u8(operand_result.address.unwrap(), self.registers.a());

				0
			}
			STX => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.write_u8(operand_result.address.unwrap(), self.registers.x());

				0
			}
			STY => {
				assert!(operand_result.address.is_some());
				//assert!(operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.write_u8(operand_result.address.unwrap(), self.registers.y());

				0
			}
			TAX => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_x(self.registers.a());

				0
			}
			TAY => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_y(self.registers.a());

				0
			}
			TSX => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_x(self.registers.s());

				0
			}
			TXA => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_a(self.registers.x());

				0
			}
			TXS => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_s(self.registers.x());

				0
			}
			TYA => {
				assert!(operand_result.address.is_none());
				assert!(!operand_result.is_page_crossed);
				assert!(operand_result.value.is_none());

				self.registers.load_a(self.registers.y());

				0
			}
			_ => {
				eprintln!(
					"{:04X}: Unimplemented operation code: {operation_code:?}",
					self.registers.pc()
				);
				//unimplemented!()
				0
			}
		}
	}

	#[inline]
	pub fn is_halted(&self) -> bool {
		self.halt
	}

	pub fn new(bus: &'a mut Bus) -> Self {
		Self {
			bus,
			cycles: 0,
			halt: false,
			registers: Registers::new(),
		}
	}

	#[must_use]
	fn read_instruction_at_pc(&mut self) -> Instruction {
		let (operation_code, addressing_mode, cycles) =
			OperationCode::LOOKUP[self.read_u8_at_pc() as usize];
		let operand = match addressing_mode {
			AddressingMode::ABS => Operand::ABS(self.read_u16_at_pc()),
			AddressingMode::ABX => Operand::ABX(self.read_u16_at_pc()),
			AddressingMode::ABY => Operand::ABY(self.read_u16_at_pc()),
			AddressingMode::ACC => Operand::ACC,
			AddressingMode::IDA => Operand::IDA(self.read_u16_at_pc()),
			AddressingMode::IDX => Operand::IDX(self.read_u8_at_pc()),
			AddressingMode::IDY => Operand::IDY(self.read_u8_at_pc()),
			AddressingMode::IMM => Operand::IMM(self.read_u8_at_pc()),
			AddressingMode::IMP => Operand::IMP,
			AddressingMode::REL => Operand::REL(self.read_u8_at_pc()),
			AddressingMode::ZPG => Operand::ZPG(self.read_u8_at_pc()),
			AddressingMode::ZPX => Operand::ZPX(self.read_u8_at_pc()),
			AddressingMode::ZPY => Operand::ZPY(self.read_u8_at_pc()),
		};

		Instruction::new(cycles, operand, operation_code)
	}

	#[must_use]
	fn read_u8(&mut self, address: u16) -> u8 {
		self.bus.cpu_read(address, false)
	}

	#[inline]
	#[must_use]
	fn read_u8_at_pc(&mut self) -> u8 {
		let address = self.registers.increment_pc();
		self.read_u8(address)
	}

	#[inline]
	#[must_use]
	fn read_u8_at_s(&mut self) -> u8 {
		let address = u16::from_le_bytes([self.registers.increment_s(), 0x01]);
		self.read_u8(address)
	}

	#[inline]
	#[must_use]
	fn read_u16(&mut self, address: u16) -> u16 {
		u16::from_le_bytes([
			self.read_u8(address.wrapping_add(0)),
			self.read_u8(address.wrapping_add(1)),
		])
	}

	#[inline]
	#[must_use]
	fn read_u16_at_pc(&mut self) -> u16 {
		u16::from_le_bytes([self.read_u8_at_pc(), self.read_u8_at_pc()])
	}

	#[inline]
	#[must_use]
	fn read_u16_at_s(&mut self) -> u16 {
		u16::from_le_bytes([self.read_u8_at_s(), self.read_u8_at_s()])
	}

	pub fn reset(&mut self) {
		//let address = self.read_u16(Self::RESET_ADDRESS);
		let address = 0xc000;
		self.registers.reset(address);
	}

	pub fn registers(&self) -> Registers {
		self.registers
	}

	pub fn registers_mut(&mut self) -> &mut Registers {
		&mut self.registers
	}

	pub fn tick(&mut self) -> u8 {
		if self.halt {
			return 0x00;
		}

		if self.cycles == 0x00 {
			let instruction = self.read_instruction_at_pc();
			let operand_result = self.execute_operand(instruction.operand());

			self.cycles = self.execute_operation_code(operand_result, instruction.operation_code())
				+ instruction.cycles();

			if self.halt {
				return 0x00;
			}
		}

		self.cycles.pre_sub_one()
	}

	fn write_u8(&mut self, address: u16, value: u8) {
		self.bus.cpu_write(address, value);
	}

	#[inline]
	fn write_u8_at_s(&mut self, value: u8) {
		let address = u16::from_le_bytes([self.registers.decrement_s(), 0x01]);
		self.write_u8(address, value);
	}

	#[inline]
	fn write_u16(&mut self, address: u16, value: u16) {
		let [lo, hi] = value.to_le_bytes();
		self.write_u8(address.wrapping_add(0), lo);
		self.write_u8(address.wrapping_add(1), hi);
	}

	#[inline]
	fn write_u16_at_s(&mut self, value: u16) {
		let [lo, hi] = value.to_le_bytes();
		self.write_u8_at_s(hi);
		self.write_u8_at_s(lo);
	}
}
