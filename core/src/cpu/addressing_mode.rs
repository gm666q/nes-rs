use crate::cpu::Operand;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum AddressingMode {
	ABS,
	ABX,
	ABY,
	ACC,
	IDA,
	IDX,
	IDY,
	IMM,
	IMP,
	REL,
	ZPG,
	ZPX,
	ZPY,
}

impl AddressingMode {
	#[inline]
	#[must_use]
	pub const fn bytes(&self) -> u8 {
		use AddressingMode::*;

		match self {
			ABS => 3,
			ABX => 3,
			ABY => 3,
			ACC => 1,
			IDA => 3,
			IDX => 2,
			IDY => 2,
			IMM => 2,
			IMP => 1,
			REL => 2,
			ZPG => 2,
			ZPX => 2,
			ZPY => 2,
		}
	}
}

impl From<Operand> for AddressingMode {
	#[inline]
	fn from(value: Operand) -> Self {
		use AddressingMode::*;

		match value {
			Operand::ABS(_) => ABS,
			Operand::ABX(_) => ABX,
			Operand::ABY(_) => ABY,
			Operand::ACC => ACC,
			Operand::IDA(_) => IDA,
			Operand::IDX(_) => IDX,
			Operand::IDY(_) => IDY,
			Operand::IMM(_) => IMM,
			Operand::IMP => IMP,
			Operand::REL(_) => REL,
			Operand::ZPG(_) => ZPG,
			Operand::ZPX(_) => ZPX,
			Operand::ZPY(_) => ZPY,
		}
	}
}
