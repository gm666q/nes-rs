use super::Status;

#[derive(Clone, Copy)]
pub struct Registers {
	a: u8,
	x: u8,
	y: u8,
	s: u8,
	p: Status,
	pc: u16,
}

impl Registers {
	#[inline]
	pub const fn a(&self) -> u8 {
		self.a
	}

	#[inline]
	pub fn branch(&mut self, flag: Status, set: bool, address: u16) -> bool {
		let result = self.p.contains(flag) == set;
		self.pc = if result { address } else { self.pc };
		result
	}

	#[inline]
	pub fn decrement_s(&mut self) -> u8 {
		let result = self.s;
		self.s = self.s.wrapping_sub(1);
		result
	}

	#[inline]
	pub fn increment_pc(&mut self) -> u16 {
		let result = self.pc;
		self.pc = self.pc.wrapping_add(1);
		result
	}

	#[inline]
	pub fn increment_s(&mut self) -> u8 {
		self.s = self.s.wrapping_add(1);
		self.s
	}

	#[inline]
	pub const fn is_flag_set(&self, flag: Status) -> bool {
		self.p.contains(flag)
	}

	#[inline]
	pub fn load_a(&mut self, value: u8) {
		self.a = value;
		self.p.set(Status::N, (self.a & 0x80) == 0x80);
		self.p.set(Status::Z, self.a == 0x00);
	}

	#[inline]
	pub fn load_p(&mut self, value: u8) {
		self.p = Status::from_bits_truncate(value).difference(Status::B);
	}

	#[inline]
	pub fn load_pc(&mut self, value: u16) {
		self.pc = value;
	}

	#[inline]
	pub fn load_s(&mut self, value: u8) {
		self.s = value;
	}

	#[inline]
	pub fn load_x(&mut self, value: u8) {
		self.x = value;
		self.p.set(Status::N, (self.x & 0x80) == 0x80);
		self.p.set(Status::Z, self.x == 0x00);
	}

	#[inline]
	pub fn load_y(&mut self, value: u8) {
		self.y = value;
		self.p.set(Status::N, (self.y & 0x80) == 0x80);
		self.p.set(Status::Z, self.y == 0x00);
	}

	#[inline]
	pub const fn new() -> Self {
		Self {
			a: 0x00,
			x: 0x00,
			y: 0x00,
			s: 0xFD,
			p: Status::empty()
				//.union(Status::B)
				.union(Status::I),
			pc: 0,
		}
	}

	#[inline]
	pub const fn offset_pc(&self, offset: u8) -> u16 {
		self.pc.wrapping_add_signed(i16::from_le_bytes([
			offset,
			if (offset & 0x80) == 0x80 { 0xff } else { 0x00 },
		]))
	}

	#[inline]
	pub const fn p(&self) -> Status {
		self.p
	}

	#[inline]
	pub const fn page(&self) -> u16 {
		self.pc & 0xff00
	}

	#[inline]
	pub const fn pc(&self) -> u16 {
		self.pc
	}

	#[inline]
	pub fn reset(&mut self, reset_address: u16) {
		//self.p.insert(Status::I);
		//self.s -= 0x03;

		self.pc = reset_address;
	}

	#[inline]
	pub const fn s(&self) -> u8 {
		self.s
	}

	#[inline]
	pub fn set_flag(&mut self, flag: Status, value: bool) {
		self.p.set(flag, value);
	}

	#[inline]
	pub const fn x(&self) -> u8 {
		self.x
	}

	#[inline]
	pub const fn y(&self) -> u8 {
		self.y
	}
}
