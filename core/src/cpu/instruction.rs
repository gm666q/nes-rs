use super::{
	AddressingMode,
	Operand::{self, *},
	OperationCode::{self, *},
};

#[derive(Clone, Copy, Debug)]
pub struct Instruction {
	cycles: u8,
	operand: Operand,
	operation_code: OperationCode,
}

impl Instruction {
	#[inline]
	pub fn addressing_mode(&self) -> AddressingMode {
		self.operand.into()
	}

	#[inline]
	pub const fn cycles(&self) -> u8 {
		self.cycles
	}

	pub const fn new(cycles: u8, operand: Operand, operation_code: OperationCode) -> Self {
		Self {
			cycles,
			operand,
			operation_code,
		}
	}

	#[inline]
	pub const fn operand(&self) -> Operand {
		self.operand
	}

	#[inline]
	pub const fn operation_code(&self) -> OperationCode {
		self.operation_code
	}
}
