use super::AddressingMode::{self, *};

use OperationCode::*;

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub enum OperationCode {
	ADC,
	ANC,
	AND,
	ARR,
	ASL,
	ASR,
	BCC,
	BCS,
	BEQ,
	BIT,
	BMI,
	BNE,
	BPL,
	BRK,
	BVC,
	BVS,
	CLC,
	CLD,
	CLI,
	CLV,
	CMP,
	CPX,
	CPY,
	DCP,
	DEC,
	DEX,
	DEY,
	EOR,
	INC,
	INX,
	INY,
	ISC,
	JAM,
	JMP,
	JSR,
	LAS,
	LAX,
	LDA,
	LDX,
	LDY,
	LSR,
	NOP,
	ORA,
	PHA,
	PHP,
	PLA,
	PLP,
	RLA,
	ROL,
	ROR,
	RRA,
	RTI,
	RTS,
	SAX,
	SBC,
	SBX,
	SEC,
	SED,
	SEI,
	SHA,
	SHS,
	SHX,
	SHY,
	SLO,
	SRE,
	STA,
	STX,
	STY,
	TAX,
	TAY,
	TSX,
	TXA,
	TXS,
	TYA,
	XAA,
}

impl OperationCode {
	pub const LOOKUP: [(Self, AddressingMode, u8); 0x100] = [
		(BRK, IMP, 7), // 0x00
		(ORA, IDX, 6), // 0x01
		(JAM, IMP, 0), // 0x02
		(SLO, IDX, 8), // 0x03
		(NOP, ZPG, 3), // 0x04
		(ORA, ZPG, 3), // 0x05
		(ASL, ZPG, 5), // 0x06
		(SLO, ZPG, 5), // 0x07
		(PHP, IMP, 3), // 0x08
		(ORA, IMM, 2), // 0x09
		(ASL, ACC, 2), // 0x0a
		(ANC, IMM, 2), // 0x0b
		(NOP, ABS, 4), // 0x0c
		(ORA, ABS, 4), // 0x0d
		(ASL, ABS, 6), // 0x0e
		(SLO, ABS, 6), // 0x0f
		(BPL, REL, 2), // 0x10
		(ORA, IDY, 5), // 0x11
		(JAM, IMP, 0), // 0x12
		(SLO, IDY, 8), // 0x13
		(NOP, ZPX, 4), // 0x14
		(ORA, ZPX, 4), // 0x15
		(ASL, ZPX, 6), // 0x16
		(SLO, ZPX, 6), // 0x17
		(CLC, IMP, 2), // 0x18
		(ORA, ABY, 4), // 0x19
		(NOP, IMP, 2), // 0x1a
		(SLO, ABY, 7), // 0x1b
		(NOP, ABX, 4), // 0x1c
		(ORA, ABX, 4), // 0x1d
		(ASL, ABX, 7), // 0x1e
		(SLO, ABX, 7), // 0x1f
		(JSR, ABS, 6), // 0x20
		(AND, IDX, 6), // 0x21
		(JAM, IMP, 0), // 0x22
		(RLA, IDX, 8), // 0x23
		(BIT, ZPG, 3), // 0x24
		(AND, ZPG, 3), // 0x25
		(ROL, ZPG, 5), // 0x26
		(RLA, ZPG, 5), // 0x27
		(PLP, IMP, 4), // 0x28
		(AND, IMM, 2), // 0x29
		(ROL, ACC, 2), // 0x2a
		(ANC, IMM, 2), // 0x2b
		(BIT, ABS, 4), // 0x2c
		(AND, ABS, 4), // 0x2d
		(ROL, ABS, 6), // 0x2e
		(RLA, ABS, 6), // 0x2f
		(BMI, REL, 2), // 0x30
		(AND, IDY, 5), // 0x31
		(JAM, IMP, 0), // 0x32
		(RLA, IDY, 8), // 0x33
		(NOP, ZPX, 4), // 0x34
		(AND, ZPX, 4), // 0x35
		(ROL, ZPX, 6), // 0x36
		(RLA, ZPX, 6), // 0x37
		(SEC, IMP, 2), // 0x38
		(AND, ABY, 4), // 0x39
		(NOP, IMP, 2), // 0x3a
		(RLA, ABY, 7), // 0x3b
		(NOP, ABX, 4), // 0x3c
		(AND, ABX, 4), // 0x3d
		(ROL, ABX, 7), // 0x3e
		(RLA, ABX, 7), // 0x3f
		(RTI, IMP, 6), // 0x40
		(EOR, IDX, 6), // 0x41
		(JAM, IMP, 0), // 0x42
		(SRE, IDX, 8), // 0x43
		(NOP, ZPG, 3), // 0x44
		(EOR, ZPG, 3), // 0x45
		(LSR, ZPG, 5), // 0x46
		(SRE, ZPG, 5), // 0x47
		(PHA, IMP, 3), // 0x48
		(EOR, IMM, 2), // 0x49
		(LSR, ACC, 2), // 0x4a
		(ASR, IMM, 2), // 0x4b
		(JMP, ABS, 3), // 0x4c
		(EOR, ABS, 4), // 0x4d
		(LSR, ABS, 6), // 0x4e
		(SRE, ABS, 6), // 0x4f
		(BVC, REL, 2), // 0x50
		(EOR, IDY, 5), // 0x51
		(JAM, IMP, 0), // 0x52
		(SRE, IDY, 8), // 0x53
		(NOP, ZPX, 4), // 0x54
		(EOR, ZPX, 4), // 0x55
		(LSR, ZPX, 6), // 0x56
		(SRE, ZPX, 6), // 0x57
		(CLI, IMP, 2), // 0x58
		(EOR, ABY, 4), // 0x59
		(NOP, IMP, 2), // 0x5a
		(SRE, ABY, 7), // 0x5b
		(NOP, ABX, 4), // 0x5c
		(EOR, ABX, 4), // 0x5d
		(LSR, ABX, 7), // 0x5e
		(SRE, ABX, 7), // 0x5f
		(RTS, IMP, 6), // 0x60
		(ADC, IDX, 6), // 0x61
		(JAM, IMP, 0), // 0x62
		(RRA, IDX, 8), // 0x63
		(NOP, ZPG, 3), // 0x64
		(ADC, ZPG, 3), // 0x65
		(ROR, ZPG, 5), // 0x66
		(RRA, ZPG, 5), // 0x67
		(PLA, IMP, 4), // 0x68
		(ADC, IMM, 2), // 0x69
		(ROR, ACC, 2), // 0x6a
		(ARR, IMM, 2), // 0x6b
		(JMP, IDA, 5), // 0x6c
		(ADC, ABS, 4), // 0x6d
		(ROR, ABS, 6), // 0x6e
		(RRA, ABS, 6), // 0x6f
		(BVS, REL, 2), // 0x70
		(ADC, IDY, 5), // 0x71
		(JAM, IMP, 0), // 0x72
		(RRA, IDY, 8), // 0x73
		(NOP, ZPX, 4), // 0x74
		(ADC, ZPX, 4), // 0x75
		(ROR, ZPX, 6), // 0x76
		(RRA, ZPX, 6), // 0x77
		(SEI, IMP, 2), // 0x78
		(ADC, ABY, 4), // 0x79
		(NOP, IMP, 2), // 0x7a
		(RRA, ABY, 7), // 0x7b
		(NOP, ABX, 4), // 0x7c
		(ADC, ABX, 4), // 0x7d
		(ROR, ABX, 7), // 0x7e
		(RRA, ABX, 7), // 0x7f
		(NOP, IMM, 2), // 0x80
		(STA, IDX, 6), // 0x81
		(NOP, IMM, 2), // 0x82
		(SAX, IDX, 6), // 0x83
		(STY, ZPG, 3), // 0x84
		(STA, ZPG, 3), // 0x85
		(STX, ZPG, 3), // 0x86
		(SAX, ZPG, 3), // 0x87
		(DEY, IMP, 2), // 0x88
		(NOP, IMM, 2), // 0x89
		(TXA, IMP, 2), // 0x8a
		(XAA, IMM, 2), // 0x8b
		(STY, ABS, 4), // 0x8c
		(STA, ABS, 4), // 0x8d
		(STX, ABS, 4), // 0x8e
		(SAX, ABS, 4), // 0x8f
		(BCC, REL, 2), // 0x90
		(STA, IDY, 6), // 0x91
		(JAM, IMP, 0), // 0x92
		(SHA, IDY, 6), // 0x93
		(STY, ZPX, 4), // 0x94
		(STA, ZPX, 4), // 0x95
		(STX, ZPY, 4), // 0x96
		(SAX, ZPY, 4), // 0x97
		(TYA, IMP, 2), // 0x98
		(STA, ABY, 5), // 0x99
		(TXS, IMP, 2), // 0x9a
		(SHS, ABY, 5), // 0x9b
		(SHY, ABX, 5), // 0x9c
		(STA, ABX, 5), // 0x9d
		(SHX, ABY, 5), // 0x9e
		(SHA, ABY, 5), // 0x9f
		(LDY, IMM, 2), // 0xa0
		(LDA, IDX, 6), // 0xa1
		(LDX, IMM, 2), // 0xa2
		(LAX, IDX, 6), // 0xa3
		(LDY, ZPG, 3), // 0xa4
		(LDA, ZPG, 3), // 0xa5
		(LDX, ZPG, 3), // 0xa6
		(LAX, ZPG, 3), // 0xa7
		(TAY, IMP, 2), // 0xa8
		(LDA, IMM, 2), // 0xa9
		(TAX, IMP, 2), // 0xaa
		(LAX, IMM, 2), // 0xab
		(LDY, ABS, 4), // 0xac
		(LDA, ABS, 4), // 0xad
		(LDX, ABS, 4), // 0xae
		(LAX, ABS, 4), // 0xaf
		(BCS, REL, 2), // 0xb0
		(LDA, IDY, 5), // 0xb1
		(JAM, IMP, 0), // 0xb2
		(LAX, IDY, 5), // 0xb3
		(LDY, ZPX, 4), // 0xb4
		(LDA, ZPX, 4), // 0xb5
		(LDX, ZPY, 4), // 0xb6
		(LAX, ZPY, 4), // 0xb7
		(CLV, IMP, 2), // 0xb8
		(LDA, ABY, 4), // 0xb9
		(TSX, IMP, 2), // 0xba
		(LAS, ABY, 4), // 0xbb
		(LDY, ABX, 4), // 0xbc
		(LDA, ABX, 4), // 0xbd
		(LDX, ABY, 4), // 0xbe
		(LAX, ABY, 4), // 0xbf
		(CPY, IMM, 2), // 0xc0
		(CMP, IDX, 6), // 0xc1
		(NOP, IMM, 2), // 0xc2
		(DCP, IDX, 8), // 0xc3
		(CPY, ZPG, 3), // 0xc4
		(CMP, ZPG, 3), // 0xc5
		(DEC, ZPG, 5), // 0xc6
		(DCP, ZPG, 5), // 0xc7
		(INY, IMP, 2), // 0xc8
		(CMP, IMM, 2), // 0xc9
		(DEX, IMP, 2), // 0xca
		(SBX, IMM, 2), // 0xcb
		(CPY, ABS, 4), // 0xcc
		(CMP, ABS, 4), // 0xcd
		(DEC, ABS, 6), // 0xce
		(DCP, ABS, 6), // 0xcf
		(BNE, REL, 2), // 0xd0
		(CMP, IDY, 5), // 0xd1
		(JAM, IMP, 0), // 0xd2
		(DCP, IDY, 8), // 0xd3
		(NOP, ZPX, 4), // 0xd4
		(CMP, ZPX, 4), // 0xd5
		(DEC, ZPX, 6), // 0xd6
		(DCP, ZPX, 6), // 0xd7
		(CLD, IMP, 2), // 0xd8
		(CMP, ABY, 4), // 0xd9
		(NOP, IMP, 2), // 0xda
		(DCP, ABY, 7), // 0xdb
		(NOP, ABX, 4), // 0xdc
		(CMP, ABX, 4), // 0xdd
		(DEC, ABX, 7), // 0xde
		(DCP, ABX, 7), // 0xdf
		(CPX, IMM, 2), // 0xe0
		(SBC, IDX, 6), // 0xe1
		(NOP, IMM, 2), // 0xe2
		(ISC, IDX, 8), // 0xe3
		(CPX, ZPG, 3), // 0xe4
		(SBC, ZPG, 3), // 0xe5
		(INC, ZPG, 5), // 0xe6
		(ISC, ZPG, 5), // 0xe7
		(INX, IMP, 2), // 0xe8
		(SBC, IMM, 2), // 0xe9
		(NOP, IMP, 2), // 0xea
		(SBC, IMM, 2), // 0xeb
		(CPX, ABS, 4), // 0xec
		(SBC, ABS, 4), // 0xed
		(INC, ABS, 6), // 0xee
		(ISC, ABS, 6), // 0xef
		(BEQ, REL, 2), // 0xf0
		(SBC, IDY, 5), // 0xf1
		(JAM, IMP, 0), // 0xf2
		(ISC, IDY, 8), // 0xf3
		(NOP, ZPX, 4), // 0xf4
		(SBC, ZPX, 4), // 0xf5
		(INC, ZPX, 6), // 0xf6
		(ISC, ZPX, 6), // 0xf7
		(SED, IMP, 2), // 0xf8
		(SBC, ABY, 4), // 0xf9
		(NOP, IMP, 2), // 0xfa
		(ISC, ABY, 7), // 0xfb
		(NOP, ABX, 4), // 0xfc
		(SBC, ABX, 4), // 0xfd
		(INC, ABX, 7), // 0xfe
		(ISC, ABX, 7), // 0xff
	];
}
