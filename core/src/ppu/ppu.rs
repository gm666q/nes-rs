use crate::Bus;

pub struct Ppu<'a> {
	bus: &'a mut Bus,
	// TODO: Finish
	registers: [u8; 8],
}

impl<'a> Ppu<'a> {
	pub fn cpu_read(&mut self, address: u16, synthetic: bool) -> u8 {
		self.registers[(address & 0x0007) as usize]
	}

	pub fn cpu_write(&mut self, address: u16, value: u8) {
		self.registers[(address & 0x0007) as usize] = value;
	}

	pub fn new(bus: &'a mut Bus) -> Self {
		Self {
			bus,
			registers: [0; 8],
		}
	}

	pub fn reset(&mut self) {}

	pub fn tick(&mut self) {}
}
