#[doc(hidden)]
pub use self::ppu::Ppu;

#[allow(clippy::module_inception)]
mod ppu;
