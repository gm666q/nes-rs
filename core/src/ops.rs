macro_rules! forward_ref_pre_post_op {
	(impl $imp:ident, $method:ident for $t:ty, $u:ty) => {
		impl $imp<&$u> for $t {
			#[inline]
			#[track_caller]
			fn $method(&mut self, other: &$u) -> $u {
				$imp::$method(self, *other)
			}
		}
	};
}

pub trait PostAdd<Rhs = Self> {
	fn post_add(&mut self, rhs: Rhs) -> Self;
}

pub trait PostAddOne {
	fn post_add_one(&mut self) -> Self;
}

macro_rules! post_add_impl {
	($(($($t:ty)*) => $one:literal),*) => ($($(
		impl PostAdd for $t {
			#[inline]
			#[track_caller]
			fn post_add(&mut self, other: $t) -> $t {
				let old = *self;
				*self += other;
				old
			}
		}

		impl PostAddOne for $t {
			#[inline]
			#[track_caller]
			fn post_add_one(&mut self) -> $t {
				self.post_add($one)
			}
		}

		forward_ref_pre_post_op! { impl PostAdd, post_add for $t, $t }
	)*)*);
}

post_add_impl! {
	(usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128) => 1,
	(f32 f64) => 1.0
}

pub trait PostSub<Rhs = Self> {
	fn post_sub(&mut self, rhs: Rhs) -> Self;
}

pub trait PostSubOne {
	fn post_sub_one(&mut self) -> Self;
}

macro_rules! post_sub_impl {
	($(($($t:ty)*) => $one:literal),*) => ($($(
		impl PostSub for $t {
			#[inline]
			#[track_caller]
			fn post_sub(&mut self, other: $t) -> $t {
				let old = *self;
				*self -= other;
				old
			}
		}

		impl PostSubOne for $t {
			#[inline]
			#[track_caller]
			fn post_sub_one(&mut self) -> $t {
				self.post_sub($one)
			}
		}

		forward_ref_pre_post_op! { impl PostSub, post_sub for $t, $t }
	)*)*);
}

post_sub_impl! {
	(usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128) => 1,
	(f32 f64) => 1.0
}

pub trait PreAdd<Rhs = Self> {
	fn pre_add(&mut self, rhs: Rhs) -> Self;
}

pub trait PreAddOne {
	fn pre_add_one(&mut self) -> Self;
}

macro_rules! pre_add_impl {
	($(($($t:ty)*) => $one:literal),*) => ($($(
		impl PreAdd for $t {
			#[inline]
			#[track_caller]
			fn pre_add(&mut self, other: $t) -> $t {
				*self += other;
				*self
			}
		}

		impl PreAddOne for $t {
			#[inline]
			#[track_caller]
			fn pre_add_one(&mut self) -> $t {
				self.pre_add($one)
			}
		}

		forward_ref_pre_post_op! { impl PreAdd, pre_add for $t, $t }
	)*)*);
}

pre_add_impl! {
	(usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128) => 1,
	(f32 f64) => 1.0
}

pub trait PreSub<Rhs = Self> {
	fn pre_sub(&mut self, rhs: Rhs) -> Self;
}

pub trait PreSubOne {
	fn pre_sub_one(&mut self) -> Self;
}

macro_rules! pre_sub_impl {
	($(($($t:ty)*) => $one:literal),*) => ($($(
		impl PreSub for $t {
			#[inline]
			#[track_caller]
			fn pre_sub(&mut self, other: $t) -> $t {
				*self -= other;
				*self
			}
		}

		impl PreSubOne for $t {
			#[inline]
			#[track_caller]
			fn pre_sub_one(&mut self) -> $t {
				self.pre_sub($one)
			}
		}

		forward_ref_pre_post_op! { impl PreSub, pre_sub for $t, $t }
	)*)*);
}

pre_sub_impl! {
	(usize u8 u16 u32 u64 u128 isize i8 i16 i32 i64 i128) => 1,
	(f32 f64) => 1.0
}
