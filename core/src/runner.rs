use std::pin::Pin;

use crate::{Bus, Debugger};

pub struct Runner<T> {
	bus: Pin<Box<Bus>>,
	debugger: T,
}

impl Runner<()> {
	pub fn new() -> Self {
		Self {
			bus: Bus::new(),
			debugger: (),
		}
	}

	pub fn reset(&mut self) {
		self.bus.as_mut().reset();
	}

	pub fn run(&mut self) -> bool {
		while self.bus.as_mut().tick() != 0 {}
		self.bus.is_halted()
	}
}

impl<T> Runner<T>
where
	T: Debugger,
{
	pub fn reset(&mut self) {
		self.debugger
			.pre_reset(unsafe { self.bus.as_mut().get_unchecked_mut() });
		self.bus.as_mut().reset();
		self.debugger
			.post_reset(unsafe { self.bus.as_mut().get_unchecked_mut() });
	}

	pub fn run(&mut self) -> bool {
		let mut ticks;

		self.debugger
			.pre_instruction(unsafe { self.bus.as_mut().get_unchecked_mut() });

		loop {
			self.debugger
				.pre_tick(unsafe { self.bus.as_mut().get_unchecked_mut() });
			ticks = self.bus.as_mut().tick();
			self.debugger
				.post_tick(unsafe { self.bus.as_mut().get_unchecked_mut() });

			if ticks == 0 {
				break;
			}
		}

		self.debugger
			.post_instruction(unsafe { self.bus.as_mut().get_unchecked_mut() });

		self.bus.is_halted()
	}

	pub fn with_debugger(debugger: T) -> Self {
		let mut this = Self {
			bus: Bus::new(),
			debugger,
		};

		this.debugger
			.init(unsafe { this.bus.as_mut().get_unchecked_mut() });

		this
	}
}
