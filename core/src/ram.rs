#[derive(Clone)]
#[repr(transparent)]
pub struct Ram([u8; Self::SIZE as _]);

impl Ram {
	const SIZE: u16 = 2 * 1024;
	const SIZE_MASK: u16 = Self::SIZE - 1;

	#[inline]
	pub const fn cpu_read(&self, address: u16, _synthetic: bool) -> u8 {
		self.0[(address & Self::SIZE_MASK) as usize]
	}

	#[inline]
	pub fn cpu_write(&mut self, address: u16, value: u8) {
		self.0[(address & Self::SIZE_MASK) as usize] = value
	}

	#[inline]
	pub const fn zeroed() -> Self {
		Self([0; Self::SIZE as _])
	}
}
