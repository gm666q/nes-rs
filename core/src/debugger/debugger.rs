use crate::Bus;

pub trait Debugger {
	fn init(&mut self, bus: &mut Bus);

	fn post_instruction(&mut self, bus: &mut Bus);

	fn post_reset(&mut self, bus: &mut Bus);

	fn post_tick(&mut self, bus: &mut Bus);

	fn pre_instruction(&mut self, bus: &mut Bus);

	fn pre_reset(&mut self, bus: &mut Bus);

	fn pre_tick(&mut self, bus: &mut Bus);
}
