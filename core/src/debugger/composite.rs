use std::{cell::RefCell, rc::Rc};

use crate::Bus;

use super::Debugger;

pub struct Composite {
	debuggers: Vec<Rc<RefCell<dyn Debugger>>>,
}

impl Composite {
	pub fn add(&mut self, debugger: Rc<RefCell<dyn Debugger>>) {
		self.debuggers.push(debugger);
	}

	pub fn new() -> Self {
		Self {
			debuggers: Vec::new(),
		}
	}
}

impl Debugger for Composite {
	fn init(&mut self, bus: &mut Bus) {
		for debugger in &self.debuggers {
			debugger.borrow_mut().init(bus);
		}
	}

	fn post_instruction(&mut self, bus: &mut Bus) {
		for debugger in &self.debuggers {
			debugger.borrow_mut().post_instruction(bus);
		}
	}

	fn post_reset(&mut self, bus: &mut Bus) {
		for debugger in &self.debuggers {
			debugger.borrow_mut().post_reset(bus);
		}
	}

	fn post_tick(&mut self, bus: &mut Bus) {
		for debugger in &self.debuggers {
			debugger.borrow_mut().post_tick(bus);
		}
	}

	fn pre_instruction(&mut self, bus: &mut Bus) {
		for debugger in &self.debuggers {
			debugger.borrow_mut().pre_instruction(bus);
		}
	}

	fn pre_reset(&mut self, bus: &mut Bus) {
		for debugger in &self.debuggers {
			debugger.borrow_mut().pre_reset(bus);
		}
	}

	fn pre_tick(&mut self, bus: &mut Bus) {
		for debugger in &self.debuggers {
			debugger.borrow_mut().pre_tick(bus);
		}
	}
}
